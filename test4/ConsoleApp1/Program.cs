﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using static System.Console;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // 確認用
            //検証1();

            // 検証用2
            //testLinq_1();

            // 検証用3 ※コーディングミスがあるので見つけてみよう！
            testLinq_2();

            Console.ReadKey();
        }

        #region おりたたみサンプル
        /// <summary>
        /// 日本語も呼べますよ。（普通は使わない）
        /// </summary>
        static void 検証1()
        {
            MessageBox.Show("熱い");

            Debug.WriteLine("aaaaaaaa");

        }
        #endregion


        /// <summary>
        /// リンクサンプル1
        /// </summary>
        /// <returns></returns>
        static bool testLinq_1()
        {

            IEnumerable<int> numbers = Enumerable.Range(1, 10);

            int sum = numbers.Sum();

            WriteLine("合計は「{0}」です。", sum);
            WriteLine($"合計は「{sum}」です。");

            return false;

        }

        /// <summary>
        /// リンクサンプル2
        /// </summary>
        /// <returns></returns>
        static bool testLinq_2()
        {

            IEnumerable<int> numbers = Enumerable.Range(0, 55);

            var results = new List<int>();
            foreach(int n in numbers)
            {
                if (n % 2 ==0)
                {
                    results.Add(n);
                }
            }
            WriteNumbers(results, "【偶数：old実装】");


            var ret = numbers.Where(n => n % 2 == 0);
            WriteNumbers(results, "【偶数：new実装】");

            return false;

        }

        /// <summary>
        /// コレクション内のすべての整数を表示するメソッド／C#6から
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers(IEnumerable<int> numbers, string header)
        {
            Write("${header}:");
            foreach (var n in numbers)
                Write($" {n}");
            WriteLine();
        }

        /// <summary>
        /// コレクション内のすべての整数を表示するメソッド／従来
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers_olstyle(IEnumerable<int> numbers, string header)
        {
            Write("{0}:", header);
            foreach (var n in numbers)
                Write(" {0}", n);
            WriteLine();
        }

    }
}
