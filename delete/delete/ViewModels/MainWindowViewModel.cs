﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using delete.Models;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace delete.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        Model model = new Model();

        public ObservableCollection<OneLine> List { get; set; } = new ObservableCollection<OneLine>();

        public void Initialize()
        {
        }

        public MainWindowViewModel()
        {
            // ここでファイルの読込などを行う想定
            List.Add(new OneLine { UserChecked = false, Name = "工程A", No = "1" });
            List.Add(new OneLine { UserChecked = false, Name = "工程B", No = "2" });
            List.Add(new OneLine { UserChecked = false, Name = "工程C", No = "3" });
            List.Add(new OneLine { UserChecked = false, Name = "工程D", No = "4" });
            List.Add(new OneLine { UserChecked = false, Name = "工程E", No = "5" });

            var listener = new PropertyChangedEventListener(model);
            listener.RegisterHandler((sender, e) => PropertyUpdateHandler(sender, e));
            this.CompositeDisposable.Add(listener);
        }

        private void PropertyUpdateHandler(object sender, PropertyChangedEventArgs e)
        {
            Debug.Print("e.PropertyName : " + e.PropertyName);
            var targetModel = sender as Model;
            switch (e.PropertyName)
            {
                default:
                    RaisePropertyChanged("");
                    break;
            }
        }

        public void delete( int index )
        {
            //List.Remove(List[index]);
            List.RemoveAt(index);
        }

        public class OneLine
        {
            public string Name { get; set; }
            public string No { get; set; }
            public bool UserChecked { get; set; }
        }
    }
}
